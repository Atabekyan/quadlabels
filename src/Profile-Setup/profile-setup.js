import React, { Component } from "react";
import Back from "../ButtonBack/back.js";
import { Redirect } from "react-router-dom";
import history from "../routes/history";
import logo from "../Profile-Setup/Profile-Setup-Images/logo.png";

export default class ProfileSetup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenCompany: false,
      isOpenUnit: false
    };
    this.openCompany = this.openCompany.bind(this);
    this.openUnit = this.openUnit.bind(this);
  }
  openCompany() {
    history.push("/profile-setup/company");
    this.setState({
      isOpenCompany: true
    });
  }
  openUnit() {
    history.push("/profile-setup/units");
    this.setState({
      isOpenUnit: true
    });
  }
  render() {
    const { isOpenCompany, isOpenUnit } = this.state;
    if (isOpenCompany) {
      return <Redirect to="/profile-setup/company" />;
    }
    if (isOpenUnit) {
      return <Redirect to="/profile-setup/units" />;
    }
    return (
      <div className="contentProfileSetup">
        <div className="containerLogoBack">
          <div className="containerLogo">
            <a href="/">
              <img src={logo} alt="No Load Image" />
            </a>
          </div>
          <Back />
        </div>
        <div className="containerUserTextButtons">
          <div className="textUser">
            <p>Profile Setup</p>
          </div>
          <div className="lineUser">
          <div className="containerUsersButtons">
            <button
              onClick={this.openCompany}
              disabled={this.props.disableCompany}
              style={{ backgroundColor: this.props.bgColorCompany }}
            >
              Company
            </button>
            <button
              onClick={this.openUnit}
              disabled={this.props.disableUnit}
              style={{ backgroundColor: this.props.bgColorUnit }}
            >
              Units
            </button>
            <button>
              <hr />
            </button>
          </div>
          </div>
        </div>
      </div>
    );
  }
}
