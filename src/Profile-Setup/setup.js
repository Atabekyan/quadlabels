import React, { Component } from "react";
import "./setup.css";
import ProfileSetup from "./profile-setup.js";
import logo from "./Profile-Setup-Images/logo.png";
import icon1 from "./Profile-Setup-Images/icon1.png";
import icon2 from "./Profile-Setup-Images/icon2.png";
import icon3 from "./Profile-Setup-Images/icon3.png";

export default class Setup extends Component {
  render() {
    return (
      <div className="contentProfileSetup">
        <div className="containerCommonProfileSetup">
          <ProfileSetup bgColorCompany="#707070" disableCompany="true" />
          <div className="containerProfileSetupCommonInputs">
            <div className="textCompanyProfileSetup">
              <p>Company Profile Setup</p>
            </div>
            <div className="profileSetupFirstInputs">
              <div className="containerProfileSetupName">
                <p>Name</p>
                <input className="cursorMove" placeholder="Quadriga USA Enterprises"></input>
              </div>
              <div className="containerProfileSetupLogo">
                <p>Logo</p>
                <input className="cursorMove" placeholder="quadriga-logo.png"></input>
                <button>Browse</button>
              </div>
            </div>
            <div className="profileSetupFirstInputsLogo">
              <img src={logo} alt="No Load Image" />
            </div>
            <div className="containerProfileSetupBranchInputs">
              <div>
                <div className="containerProfileSetupBranch">
                  <p>Branch</p>
                  <input className="cursorMove" placeholder="Manin"></input>
                  <div className="containerCustomersCustomersButtons">
                    <button>
                      <img src={icon1} alt="No Load Image" />
                    </button>
                    <button>
                      <img src={icon3} alt="No Load Image" />
                    </button>
                  </div>
                </div>
                <div className="containerProfileSetupAddress">
                  <p>Address</p>
                  <input className="cursorMove" placeholder="28410 Witherspoon Pkwy"></input>
                </div>
                <div className="containerProfileSetupCity">
                  <p>City</p>
                  <input className="cursorMove" placeholder="Glendale"></input>
                </div>
                <div className="containerProfileSetupZip">
                  <p>Zip</p>
                  <input className="cursorMove" placeholder="91201"></input>
                </div>
                <div className="containerProfileSetupState">
                  <p>State</p>
                  <input className="cursorMove" placeholder="CA"></input>
                </div>
                <div className="containerProfileSetupContactPerson">
                  <p>Contact Person</p>
                  <input className="cursorMove" placeholder="Tom Johns"></input>
                </div>
                <div className="containerProfileSetupPhone">
                  <p>Phone</p>
                  <input className="cursorMove" placeholder="(818) 633 9094"></input>
                </div>
                <div className="containerProfileSetupEmail">
                  <p>e-mail</p>
                  <input className="cursorMove" placeholder="tj@gmail.com"></input>
                </div>
              </div>
            </div>
            <div className="containerButtonSaveCSRs">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
