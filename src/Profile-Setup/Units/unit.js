import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import ProfileSetup from "../profile-setup.js";

export default class Unit extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="contentUnit">
        <div className="containerCommonUnit">
          <ProfileSetup bgColorUnit="#707070" disableUnit="true" />
          <div className="containerUnitTextCheckbox">
            <div className="containerUnitCheckboxText">
              <div className="textUnits">
                <p>Units</p>
              </div>
              <div className="containerLabelSize">
                <p>Label Size</p>
                <div className="LabelSizemm">
                  <input type="radio" name="1"></input>
                  <p>mm</p>
                </div>
                <div className="LabelSizeinch">
                  <input  type="radio" name="1"></input>
                  <p>inch</p>
                </div>
              </div>
              <div className="containerCenterToCenterSize">
                <p>Center to Center Size</p>
                <div className="CenterToCentermm">
                  <input  type="radio" name="2"></input>
                  <p>mm</p>
                </div>
                <div className="CenterToCenterinch">
                  <input type="radio" name="2"></input>
                  <p>inch</p>
                </div>
              </div>
              <div className="containerGapSize">
                <p>Gap Size</p>
                <div className="GapSizemm">
                  <input type="radio" name="3"></input>
                  <p>mm</p>
                </div>
                <div className="GapSizeinch">
                  <input type="radio" name="3"></input>
                  <p>inch</p>
                </div>
              </div>
              <div className="containerDieTickness">
                <p>Die Tickness</p>
                <div className="DieTicknessmm">
                  <input type="radio" name="4"></input>
                  <p>mm</p>
                </div>
                <div className="DieTicknessmil">
                  <input type="radio" name="4"></input>
                  <p>mil</p>
                </div>
              </div>
              <div className="containerRollLength">
                <p>Roll Length</p>
                <div className="RollLengthmm">
                  <input type="radio" name="5"></input>
                  <p>mm</p>
                </div>
                <div className="RollLengthft">
                  <input type="radio" name="5"></input>
                  <p>ft</p>
                </div>
              </div>
              <div className="containerRollWidth">
                <p>Roll Width</p>
                <div className="RollWidthmm">
                  <input type="radio" name="6"></input>
                  <p>mm</p>
                </div>
                <div className="RollWidthinch">
                  <input type="radio" name="6"></input>
                  <p>inch</p>
                </div>
              </div>
              <div className="containerMaterialTickness">
                <p>Material Tickness</p>
                <div className="MaterialTicknessmm">
                  <input type="radio" name="7"></input>
                  <p>mm</p>
                </div>
                <div className="MaterialTicknessmil">
                  <input type="radio" name="7"></input>
                  <p>mil</p>
                </div>
              </div>
              <div className="containerMaterialPricinkper">
                <p>Material Pricink per</p>
                <div className="MaterialPricinkpersqm">
                  <input type="radio" name="8"></input>
                  <p>sqm</p>
                </div>
                <div className="MaterialPricinkpermsi">
                  <input type="radio" name="8"></input>
                  <p>msi</p>
                </div>
              </div>
              <div className="containerVarnishTickness">
                <p>Varnish Tickness</p>
                <div className="VarnishTicknessmicron">
                  <input type="radio" name="9"></input>
                  <p>micron</p>
                </div>
                <div className="VarnishTicknessmil">
                  <input type="radio" name="9"></input>
                  <p>mil</p>
                </div>
              </div>
              <div className="containerLabelWeight">
                <p>Label Weight</p>
                <div className="LabelWeightkg">
                  <input type="radio" name="10"></input>
                  <p>kg/sqm</p>
                </div>
                <div className="LabelWeightlb">
                  <input type="radio" name="10"></input>
                  <p>lb/msi</p>
                </div>
              </div>
              <div className="containerVariableDataPricingper">
                <p>Variable Data Pricing per</p>
                <div className="VariableDataPricingpernumber">
                  <input type="radio" name="11"></input>
                  <p>number</p>
                </div>
                <div className="VariableDataPricingpermsi">
                  <input type="radio" name="11"></input>
                  <p>msi</p>
                </div>
              </div>
              <div className="containerLIIIII">
                <p>LIIIII</p>
                <div className="LIIIIIkg">
                  <input type="radio" name="12"></input>
                  <p>kg/sqm</p>
                </div>
                <div className="LIIIIIlb">
                  <input type="radio" name="12"></input>
                  <p>lb/msi</p>
                </div>
              </div>
              <div className="containerUnitLastCheckbox">
                <p>Currency</p>
                <div className="CurrencyUSD">
                  <input type="radio" name="13"></input>
                  <p>USD</p>
                </div>
                <div className="CurrencyEuro">
                  <input type="radio" name="13"></input>
                  <p>Euro</p>
                </div>
                <div className="CurrencyRate">
                  <p>Rate</p>
                  <input className="cursorMove" placeholder="1.18"></input>
                </div>
                <div className="CurrencyName">
                  <p>Name</p>
                  <input className="cursorMove" placeholder="Euro"></input>
                </div>
              </div>
              <div className="containerButtonSaveCSRs">
                <button>Save</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
