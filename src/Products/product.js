import React, { Component } from "react";
import Back from "../ButtonBack/back.js";
import Select from "react-select";
import Logo from "../Logo/logo.js";
import icon1 from "./Product-Images/icon1.png";
import icon2 from "./Product-Images/icon2.png";
import icon3 from "./Product-Images/icon3.png";
import icon4 from "./Product-Images/icon4.png";

export default class Product extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const options = [
      { label: "Pacer Print", value: 1 },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentProduct">
        <div className="containerLogoBack">
          <Logo display="none" />
          <Back />
        </div>
        <div className="containerCommonProduct">
          <div className="textProduct">
            <p>Products</p>
          </div>
          <div className="containerCommonProductInputs">
            <div className="textProductMenuSetup">
              <p>Product Menu Setup</p>
            </div>
            <div className="containerProductMenuSetup">
              <div className="containerProductMenuProduct">
                <p>Product Menu </p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameProduct">
                <p>Name</p>
                <input
                  className="cursorMove"
                  placeholder="Synthetic Labels"
                ></input>
              </div>
            </div>
            <div className="textLabelSpecifications">
              <p>Label Specifications</p>
            </div>
            <div className="containerLabelSpecifications">
              <div className="containerMaterialTypes">
                <p>Material types</p>
                <div className="containerSelectImages">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerShapeTypes">
                <p>Shape types</p>
                <div className="containerSelectImages">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerColor">
                <p>Color</p>
                <div className="containerSelectImages">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerPicture">
                <p>Picture</p>
                <input
                  className="cursorMove"
                  placeholder="whitePP-LabelSpec.png"
                ></input>
                <div className="buttonBrowseLabelSpecification">
                  <button>Browse</button>
                </div>
              </div>
              <div className="containerLabelSpecificationsPicture">
                <img src="roll.png" alt="No Load Imge"></img>
              </div>
              <div className="containerCommentsProduct">
                <p>Info/Comments</p>
                <textarea
                  className="cursorMoveTopProduct"
                  placeholder="Info/Comments"
                ></textarea>
              </div>
            </div>
            <div className="textLabelOptions">
              <p>Label Options</p>
            </div>
            <div className="containerLabelOptions">
              <div className="containerSurfaceFinishing">
                <p>Surface Finishing</p>
                <div className="containerSelectIcons">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerSpotUvVarnish">
                <p>Spot UV Varnish</p>
                <div className="containerSelectIcons">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerFoilStamping">
                <p>Foil Stampming</p>
                <div className="containerSelectIcons">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerVariableData">
                <p>Variable Data</p>
                <div className="containerSelectIcons">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerPictureProduct">
                <p>Picture</p>
                <input
                  className="cursorMove"
                  placeholder="WhitePP-LabelOptions.png"
                ></input>
                <div className="containerbuttonBrowseLabelOptions">
                  <button>Browse</button>
                </div>
              </div>
              <div className="containerLabelOptionsRectagle">
                <img src="mask-group.png"></img>
              </div>
              <div className="containerCommentsLabelLocations">
                <p>Info/Comments</p>
                <textarea
                  className="cursorMoveTopProduct"
                  placeholder="Info/Comments"
                ></textarea>
              </div>
            </div>
            <div className="textRollParametersProduct">
              <p>Roll Parameters</p>
            </div>
            <div className="containerRollParameters">
              <div className="containerRollFinishing">
                <p>Roll Finishing</p>
                <div className="containerSelectIcon">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerLabelDirection">
                <p>Label Direction</p>
                <div className="containerSelectIcon">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerLabelsPerRoll">
                <p>Labels Per Roll</p>
                <div className="containerSelectIcon">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerLabelsPerStack">
                <p>Labels Per Stack</p>
                <div className="containerSelectIcon">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerMaximumOd">
                <p>Maximum OD</p>
                <div className="containerSelectIcon">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerCoreDiameter">
                <p>Core Diameter</p>
                <div className="containerSelectIcon">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerSurfaceFinishingSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerPicturePicture">
                <p>Picture</p>
                <input
                  className="cursorMove"
                  placeholder="WhitePP-LabelOptions.png"
                ></input>
                <div className="containerbuttonBrowsePicture">
                  <button>Browse</button>
                </div>
              </div>
              <div className="containerRollParametersPicture">
                <img src="R-CW-0.png" alt="No Load Imge"></img>
              </div>
              <div className="containerCommentRollFinishing">
                <p>Info/Comments</p>
                <textarea
                  className="cursorMoveTopProduct"
                  placeholder="Info/Comments"
                ></textarea>
              </div>
            </div>
            <div className="textQuantities">
              <p>Quantities</p>
            </div>
            <div className="containerQuantities">
              <div className="containerProductMenu">
                <p>Product Menu</p>
                <div className="containerProductMenuInputs">
                  <div className="containerProductMenuInputsFirst">
                    <input className="cursorMove" placeholder="250"></input>
                    <div className="containerCustomersCustomersButtons">
                      <button>
                        <img src={icon1} alt="No Load Image" />
                      </button>
                    </div>
                  </div>
                  <div className="containerProductMenuInputsSecond">
                    <input className="cursorMove" placeholder="500"></input>
                    <div className="containerCustomersCustomersButtons">
                      <button>
                        <img src={icon1} alt="No Load Image" />
                      </button>
                    </div>
                  </div>
                  <div className="containerProductMenuInputsThirdy">
                    <input className="cursorMove" placeholder="1000"></input>
                    <div className="containerCustomersCustomersButtons">
                      <button>
                        <img src={icon1} alt="No Load Image" />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="containerPictureQuantities">
                <p>Picture</p>
                <input
                  className="cursorMove"
                  placeholder="WhitePP-LabelOptions.png"
                ></input>
                <div className="containerbuttonBrowseLabelQuantities">
                  <button>Browse</button>
                </div>
              </div>
              <div className="containerQuantitiesPicture">
                <img src="R-CW-0.png" alt="No Load Imge"></img>
              </div>
              <div className="containerCommentsQuantities">
                <p>Info/Comments</p>
                <textarea
                  className="cursorMoveTopProduct"
                  placeholder="Info/Comments"
                ></textarea>
              </div>
            </div>
            <div className="textOrderData">
              <p>Ordrer Data</p>
            </div>
            <div className="containerOrderData">
              <div className="containerJobName">
                <p>Job Name</p>
                <input type="checkbox"></input>
              </div>
              <div className="containerProductionSampleProduct">
                <p>Producion Sample</p>
                <input type="checkbox"></input>
                <div className="containerProductionSampleSelectIcons">
                  <input className="SelectCheckbox" type="checkbox"></input>
                  <button className="SelectButton">
                    <img src={icon4} />
                  </button>
                </div>
                <div className="containerOrderDataSelect">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerArtworkProduct">
                <p>Artwork(s)</p>
                <input type="checkbox"></input>
                <div className="containerMaxArtwork">
                  <p>Max Artwork(s)</p>
                  <input className="cursorMove" placeholder="25"></input>
                  <div className="containerArtworkDefault">
                    <p>Default</p>
                    <input className="cursorMove" placeholder="1"></input>
                  </div>
                </div>
              </div>
              <div className="containerShoppingMethod">
                <p>Shopping Method</p>
                <input type="checkbox"></input>
                <div className="containerShoppingMethodSelect">
                  <div className="containerSelectIconShoppingMethod">
                    <input className="SelectCheckbox" type="checkbox"></input>
                    <button className="SelectButton">
                      <img src={icon4} />
                    </button>
                    <div className="containerShoppingMethodSelect">
                      <Select options={options} />
                    </div>
                  </div>
                  <div className="containerSelectSecondIconShoppingMethod">
                    <input className="SelectCheckbox" type="checkbox"></input>
                    <button className="SelectButton">
                      <img src={icon4} />
                    </button>
                    <div className="containerhoppingMethodSecondSelect">
                      <Select options={options} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="contanerNotes">
                <p>Notes</p>
                <input type="checkbox"></input>
              </div>
            </div>
            <div className="containerbuttonSaveProducts">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
