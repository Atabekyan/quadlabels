import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import history from "../routes/history";
import Back from "../ButtonBack/back.js";
import Logo from "../Logo/logo.js";

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenLocation: false,
      isOpenUser: false,
      isOpenProduct: false,
      isOpenTerminology: false,
      isOpenSetup: false,
      isOpenGeneralSetup: false,
      isOpenMenuSetup: false
    };
    this.openLocation = this.openLocation.bind(this);
    this.openUser = this.openUser.bind(this);
    this.openProduct = this.openProduct.bind(this);
    this.openTerminology = this.openTerminology.bind(this);
    this.openSetup = this.openSetup.bind(this);
    this.openGeneralSetup = this.openGeneralSetup.bind(this);
    this.openMenuSetup = this.openMenuSetup.bind(this);
  }
  openLocation() {
    history.push("/locations");
    this.setState({
      isOpenLocation: true
    });
  }
  openUser() {
    history.push("/users/customers");
    this.setState({
      isOpenUser: true
    });
    document.getElementsByName("Products").backgroundColor = "lightblue";
  }
  openProduct() {
    history.push("/products");
    this.setState({
      isOpenProduct: true
    });
  }
  openTerminology() {
    history.push("/terminology");
    this.setState({
      isOpenTerminology: true
    });
  }
  openSetup() {
    history.push("/profile-setup/company");
    this.setState({
      isOpenSetup: true
    });
  }
  openGeneralSetup() {
    history.push("/general-setup/utilities");
    this.setState({
      isOpenGeneralSetup: true
    });
  }
  openMenuSetup() {
    history.push("/menu-setup/materials");
    this.setState({
      isOpenMenuSetup: true
    });
  }
  render() {
    const {
      isOpenLocation,
      isOpenUser,
      isOpenProduct,
      isOpenTerminology,
      isOpenSetup,
      isOpenGeneralSetup,
      isOpenMenuSetup
    } = this.state;
    if (isOpenLocation) {
      return <Redirect to="/locations" />;
    }
    if (isOpenUser) {
      return <Redirect to="/users/customers" />;
    }
    if (isOpenProduct) {
      return <Redirect to="/products" />;
    }
    if (isOpenTerminology) {
      return <Redirect to="/terminology" />;
    }
    if (isOpenSetup) {
      return <Redirect to="/profile-setup/company" />;
    }
    if (isOpenGeneralSetup) {
      return <Redirect to="/general-setup/utilities" />;
    }
    if (isOpenMenuSetup) {
      return <Redirect to="/menu-setup/materials" />;
    }
    return (
      <div className="contentDashboard">
        <Logo />
        <div className="containerDashboardButtonText">
        <div className="textDashboard">
          <p>Dashboard</p>
        </div>
        <div className="Line">
          <div className="containerDashboardButtons">
            <button id="dashboard">Dashboard</button>
            <button onClick={this.openUser}>Users</button>
            <button onClick={this.openProduct}>Products</button>
            <button onClick={this.openMenuSetup}>Menu Setup</button>
            <button>Developer Setup</button>
            <button onClick={this.openGeneralSetup}>General Setup</button>
            <button onClick={this.openSetup}>Profile Setup</button>
            <button onClick={this.openTerminology}>Terminology</button>
            <button onClick={this.openLocation}>Locations</button>
          </div>
        </div>
        </div>
      </div>
    );
  }
}
