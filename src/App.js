import React from "react";
import "./App.css";
import Logo from "./Logo/logo.js";
import Dashboard from "./Dashboard/dashboard.js";
import Location from './Locations/location.js';
import User from './Users/user.js';
import Customer from './Users/Customers/customer.js';
import CSRs from './Users/CSRs/csrs.js';
import Operator from './Users/Operators/operator.js';
import Administrator from './Users/Administrators/administrator.js';
import Calculator from './Users/Calculator/calculator.js';
import Product from './Products/product.js';
import Terminology from './Terminilogy/terminology.js';
import Setup from './Profile-Setup/setup.js';
import Unit from './Profile-Setup/Units/unit.js';
import GeneralSetup from './General-Setup/general-setup.js';
import ProductionSets from './General-Setup/Production-Sets/production-sets.js';
import Utilities from './General-Setup/Utilities/utilities.js'
import Profitability from "./General-Setup/Profitability/profitability.js";
import MenuSetup from './Menu-Setup/menu-setup.js';
import Material from './Menu-Setup/Material/material.js';
import Color from './Menu-Setup/Colors/color.js';
import Dies from './Menu-Setup/Dies/dies.js';
import SurfaceFinishing from './Menu-Setup/Surface-Finishing/surface-finishing.js';
import FoilStamping from './Menu-Setup/Foil-Stamping/foil-stamping.js';
import SpotUvVarnish from './Menu-Setup/Spot-UV-Varnish/spot-uv-varnish.js';
import VariableData from './Menu-Setup/Variable-data/variable-data.js';
import OrderData from './Menu-Setup/Order-Data/order-data.js';
import RollParameter from './Menu-Setup/Roll-Parameters/roll-parameters.js';
import Routes from './routes/Routes.js';
import "./Logo/logo.css";
import "./Dashboard/dashboard.css";
import "./Locations/location.css";
import './Users/user.css';
import './Users/customer.css';
import './Users/CSRs/csrs.css';
import './Users/Operators/operator.css';
import './Users/Administrators/administrator.css';
import './Users/Calculator/calculator.css';
import './Products/product.css';
import './Terminilogy/terminology.css';
import './Profile-Setup/setup.css';
import './Profile-Setup/Units/unit.css';
import './General-Setup/general-setup.css';
import './General-Setup/Production-Sets/production-sets.css';
import './General-Setup/Utilities/utilities.css';
import './General-Setup/Profitability/profitability.css';
import './Menu-Setup/menu-setup.css';
import './Menu-Setup/Material/material.css';
import './Menu-Setup/Colors/color.css';
import './Menu-Setup/Dies/dies.css';
import './Menu-Setup/Surface-Finishing/surface-finishing.css';
import './Menu-Setup/Foil-Stamping/foil-stamping.css';
import './Menu-Setup/Spot-UV-Varnish/spot-uv-varnish.css';
import './Menu-Setup/Variable-data/variable-data.css';
import './Menu-Setup/Order-Data/order-data.css';
import './Menu-Setup/Roll-Parameters/roll-parameters.css';
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";

class App extends React.Component {
  render() {
    return (
       <div className="containerApp">
          <Routes />
       </div>
    );
  }
}

export default App;
