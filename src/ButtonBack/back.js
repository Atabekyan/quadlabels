import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

export default class Back extends Component{
    constructor(props){
        super(props);
        this.state={
            isClickBack: false,
        }
        this.backHome = this.backHome.bind(this);
    }
    backHome(){
        this.setState({
            isClickBack: true
        })
    }
    render(){
        const { isClickBack } = this.state;
        if(isClickBack){
            return <Redirect to ="/" />
        }
        return (
            <div className="contentBack"> 
                <div className="buttonBack">
                    <button style={{display:this.props.display}} onClick={this.backHome}>Back</button>
                </div>
            </div>
        )
    }
}
