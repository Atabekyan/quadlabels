import React from "react";

import Logo from "../Logo/logo.js";
import Dashboard from "../Dashboard/dashboard.js";
import Location from "../Locations/location.js";
import User from "../Users/user.js";
import Customer from "../Users/Customers/customer.js";
import CSRs from "../Users/CSRs/csrs.js";
import Operator from "../Users/Operators/operator.js";
import Administrator from "../Users/Administrators/administrator.js";
import Calculator from "../Users/Calculator/calculator.js";
import Product from "../Products/product.js";
import Terminology from "../Terminilogy/terminology.js";
import Setup from "../Profile-Setup/setup.js";
import Unit from "../Profile-Setup/Units/unit.js";
import GeneralSetup from "../General-Setup/general-setup.js";
import ProductionSets from "../General-Setup/Production-Sets/production-sets.js";
import Utilities from "../General-Setup/Utilities/utilities.js";
import Profitability from "../General-Setup/Profitability/profitability.js";
import MenuSetup from "../Menu-Setup/menu-setup.js";
import Material from "../Menu-Setup/Material/material.js";
import Color from "../Menu-Setup/Colors/color.js";
import Dies from "../Menu-Setup/Dies/dies.js";
import SurfaceFinishing from "../Menu-Setup/Surface-Finishing/surface-finishing.js";
import FoilStamping from "../Menu-Setup/Foil-Stamping/foil-stamping.js";
import SpotUvVarnish from "../Menu-Setup/Spot-UV-Varnish/spot-uv-varnish.js";
import VariableData from "../Menu-Setup/Variable-data/variable-data.js";
import OrderData from "../Menu-Setup/Order-Data/order-data.js";
import RollParameter from "../Menu-Setup/Roll-Parameters/roll-parameters.js";
import Back from "../ButtonBack/back.js";
import "../Logo/logo.css";
import "../Dashboard/dashboard.css";
import "../Locations/location.css";
import "../Users/user.css";
import "../Users/customer.css";
import "../Users/CSRs/csrs.css";
import "../Users/Operators/operator.css";
import "../Users/Administrators/administrator.css";
import "../Users/Calculator/calculator.css";
import "../Products/product.css";
import "../Terminilogy/terminology.css";
import "../Profile-Setup/setup.css";
import "../Profile-Setup/Units/unit.css";
import "../General-Setup/general-setup.css";
import "../General-Setup/Production-Sets/production-sets.css";
import "../General-Setup/Utilities/utilities.css";
import "../General-Setup/Profitability/profitability.css";
import "../Menu-Setup/menu-setup.css";
import "../Menu-Setup/Material/material.css";
import "../Menu-Setup/Colors/color.css";
import "../Menu-Setup/Dies/dies.css";
import "../Menu-Setup/Surface-Finishing/surface-finishing.css";
import "../Menu-Setup/Foil-Stamping/foil-stamping.css";
import "../Menu-Setup/Spot-UV-Varnish/spot-uv-varnish.css";
import "../Menu-Setup/Variable-data/variable-data.css";
import "../Menu-Setup/Order-Data/order-data.css";
import "../Menu-Setup/Roll-Parameters/roll-parameters.css";
import "../ButtonBack/back.css";
import { BrowserRouter, Switch } from "react-router-dom";
import { Component } from "react";
import { Router, Route, Link } from "react-router-dom";
import history from "./history";
import './Routes.css';

export default class Routes extends Component {
  render() {
    return (
      <div className="contentRoutes">
        <Switch>
          <Router history={history}>
            <div className="App">
              <div className="containerLogoMenu">
                <Route exact path="/">
                  <Dashboard />
                </Route>
                <div className="containerRoutes">
                <Route exact path="/users/customers" component={Customer} />
                <Route exact path="/users/csrs" component={CSRs} />
                <Route exact path="/users/operators" component={Operator} />
                <Route path="/locations">
                  <Location />
                </Route>
                <Route
                  exact
                  path="/users/administrators"
                  component={Administrator}
                />
                <Route
                  exact
                  path="/users/customers/calculator"
                  component={Calculator}
                />
                <Route exact path="/products" component={Product} />
                <Route exact path="/terminology" component={Terminology} />
                <Route exact path="/profile-setup/company" component={Setup} />
                <Route exact path="/profile-setup/units" component={Unit} />
                <Route exact path="/general-setup/utilities" component={Utilities} />
                <Route
                  exact
                  path="/general-setup/profabilities"
                  component={Profitability}
                />
                <Route
                  exact
                  path="/general-setup/production-sets"
                  component={ProductionSets}
                />
                <Route exact path="/menu-setup" component={MenuSetup} />
                <Route
                  exact
                  path="/menu-setup/materials"
                  component={Material}
                />
                <Route exact path="/menu-setup/colors" component={Color} />
                <Route exact path="/menu-setup/dies" component={Dies} />
                <Route
                  exact
                  path="/menu-setup/surface-finishing"
                  component={SurfaceFinishing}
                />
                <Route
                  exact
                  path="/menu-setup/foil-stamping"
                  component={FoilStamping}
                />
                <Route
                  exact
                  path="/menu-setup/spot-uv-varnish"
                  component={SpotUvVarnish}
                />
                <Route
                  path="/menu-setup/variable-data"
                  component={VariableData}
                />
                <Route
                  path="/menu-setup/order-data"
                  component={OrderData}
                />
                <Route
                  path="/menu-setup/roll-parameter"
                  component={RollParameter}
                />
                </div>               
              </div>
            </div>
          </Router>
        </Switch>
      </div>
    );
  }
}
