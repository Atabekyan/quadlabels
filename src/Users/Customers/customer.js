import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import history from "../../routes/history";
import User from "../user.js";
import Select from "react-select";
import logo from "../User-Images/logo.png";
import icon1 from "../User-Images/icon1.png";
import icon2 from "../User-Images/icon2.png";
import icon3 from "../User-Images/icon3.png";

export default class Customer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenCalculator: false,
      isClickAddIcon: false
    };
    this.openCalculator = this.openCalculator.bind(this);
    this.addInput = this.addInput.bind(this);
  }
  openCalculator() {
    history.push("/users/customers/calculator");
    this.setState({
      isOpenCalculator: true
    });
  }
  addInput() {
    return (
      <div className="containerLogoCustomers">
        <p>Logo</p>
        <div className="containerSellersPermitInput">
          <input className="cursorMove" placeholder="Pacer-logo.png"></input>
        </div>
        <button>Browse</button>
      </div>
    );
  }
  render() {
    const options = [
      { label: "Pacer Print", value: "" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 2 },
      { label: "Pacer Vrint", value: 3 }
    ];
    const { isOpenCalculator } = this.state;
    if (isOpenCalculator) {
      return <Redirect to="/users/customers/calculator" />;
    }
    return (
      <div className="contentCustomer">
        <div className="containerCommonCustomer">
          <User bgColorCustomer="#707070" disableCustomer="true" />
          <div className="containerCustomerCommonInputs">
            <div className="textCustomers">
              <p>Customers</p>
            </div>
            <div className="containerCustomersCategory">
              <div className="containerCategoryCustomers">
                <p>Category</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerCustomersCustomers">
                <p>Customers</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerCompanyNameCustomers">
                <p>Company Name</p>
                <input className="cursorMove" placeholder="Pacer Print"></input>
              </div>
              <div className="containerSellersPermitCustomers">
                <p>Seller's Permit</p>
                <div className="containerSellersPermitInputButton">
                  <input
                    className="cursorMove"
                    placeholder="Pacer-sellers permit.jpg"
                  ></input>
                  <button>Download</button>
                </div>
              </div>
              <div className="containerDiscountRateCustomers">
                <p>Discount Rate</p>
                <div className="containerSellersPermitInputButton">
                  <input className="cursorMove" placeholder="15.8 %"></input>
                  <button onClick={this.openCalculator}>Calculate</button>
                </div>
              </div>
              <div
                className="containerDefaultCustomers"
                className="containerCorrectAlignItems"
              >
                <p>Default CPrM</p>
                <input className="cursorMove" placeholder="25%"></input>
              </div>
              <div className="containerPaymentCustomers">
                <p>Payment Option</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerButtonSubmit">
                  <button>Submit</button>
                </div>
              </div>
              <div className="containerLogoCustomers">
                <p>Logo</p>
                <div className="containerSellersPermitInputButton">
                  <input
                    className="cursorMove"
                    placeholder="Pacer-logo.png"
                  ></input>
                  <button>Browse</button>
                </div>
              </div>
              <div className="customerFirstInputsLogo">
                <img src={logo} alt="No load" />
              </div>
            </div>
            <div className="containerAccountCustomersSecondInputs" id="style-1">
              <div className="containerAccountCustomers">
                <p>Account</p>
                <div className="containerAccountCustomersInput">
                  <input
                    className="cursorMove"
                    placeholder="Tom Johns account"
                  ></input>
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button onClick={this.addInput}>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div
                className="containerAddressCustomers"
                className="containerCorrectAlignItems"
              >
                <p>Address</p>
                <input
                  className="cursorMove"
                  placeholder="1151 Justin Ave"
                ></input>
              </div>
              <div
                className="containerEmptyCustomers"
                className="containerCorrectAlignItems"
              >
                <p></p>
                <input className="cursorMove"></input>
              </div>
              <div
                className="containerCityCustomers"
                className="containerCorrectAlignItems"
              >
                <p>City</p>
                <input className="cursorMove" placeholder="Glendale"></input>
              </div>
              <div
                className="containerZipCustomers"
                className="containerCorrectAlignItems"
              >
                <p>ZIP</p>
                <input className="cursorMove" placeholder="91201"></input>
              </div>
              <div
                className="containerStateCustomers"
                className="containerCorrectAlignItems"
              >
                <p>State</p>
                <input className="cursorMove" placeholder="CA"></input>
              </div>
              <div
                className="containerContactPersonCustomers"
                className="containerCorrectAlignItems"
              >
                <p>Contact Person</p>
                <input className="cursorMove" placeholder="Tom Jhons"></input>
              </div>
              <div
                className="containerPhoneCustomers"
                className="containerCorrectAlignItems"
              >
                <p>Phone</p>
                <input
                  className="cursorMove"
                  placeholder="(818) 633 9094"
                ></input>
              </div>
              <div
                className="containerEmailCustomers"
                className="containerCorrectAlignItems"
              >
                <p>e-mail</p>
                <input
                  className="cursorMove"
                  placeholder="tj@gmail.com"
                ></input>
              </div>
              <div
                className="containerEmailCustomers"
                className="containerCorrectAlignItems"
              >
                <p>e-mail</p>
                <input
                  className="cursorMove"
                  placeholder="tj@gmail.com"
                ></input>
              </div>
              <div
                className="containerEmailCustomers"
                className="containerCorrectAlignItems"
              >
                <p>e-mail</p>
                <input
                  className="cursorMove"
                  placeholder="tj@gmail.com"
                ></input>
              </div>
              <div
                className="containerEmailCustomers"
                className="containerCorrectAlignItems"
              >
                <p>e-mail</p>
                <input
                  className="cursorMove"
                  placeholder="tj@gmail.com"
                ></input>
              </div>
            </div>
            <div className="GrantAccessButton">
              <button>Grant Access</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
