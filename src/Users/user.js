import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import history from "../routes/history";
import Back from "../ButtonBack/back.js";
import Logo from "../Logo/logo.js";
import logo from "./User-Images/logo.png";

export default class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenCustomer: false,
      isOpenCSRs: false,
      isOpenOperator: false,
      isOpenAdministrator: false
    };
    this.openCustomers = this.openCustomers.bind(this);
    this.openCSRs = this.openCSRs.bind(this);
    this.openOperators = this.openOperators.bind(this);
    this.openAdministrators = this.openAdministrators.bind(this);
  }
  openCustomers() {
    history.push("/users/customers");
    this.setState({
      isOpenCustomer: true
    });
  }
  openCSRs() {
    history.push("/users/csrs");
    this.setState({
      isOpenCSRs: true
    });
  }
  openOperators() {
    history.push("/users/operators");
    this.setState({
      isOpenOperator: true
    });
  }
  openAdministrators() {
    history.push("/users/administrators");
    this.setState({
      isOpenAdministrator: true
    });
  }
  render() {
    const {
      isOpenCustomer,
      isOpenCSRs,
      isOpenOperator,
      isOpenAdministrator
    } = this.state;
    if (isOpenCustomer) {
      return <Redirect to="/users/customers" />;
    }
    if (isOpenCSRs) {
      return <Redirect to="/users/csrs" />;
    }
    if (isOpenOperator) {
      return <Redirect to="/users/operators" />;
    }
    if (isOpenAdministrator) {
      return <Redirect to="/users/administrators" />;
    }
    return (
      <div className="contentUser">
        <div className="containerLogoBack">
        <div className="containerLogo">
            <a href="/">
              <img src={logo} alt="No Load Image" />
            </a>
          </div>
          <Back />
        </div>
        <div className="containerUserTextButtons">
        <div className="textUser">
          <p>Users</p>
        </div>
        <div className="lineUser">
          <div className="containerUsersButtons">
            <button
              disabled={this.props.disableCustomer}
              style={{ backgroundColor: this.props.bgColorCustomer }}
              onClick={this.openCustomers}
            >
              Customers
            </button>
            <button
              disabled={this.props.disableCSRS}
              style={{ backgroundColor: this.props.bgColorCSRS }}
              onClick={this.openCSRs}
            >
              CSRs
            </button>
            <button
              disabled={this.props.disableOperator}
              style={{ backgroundColor: this.props.bgColorOperator }}
              onClick={this.openOperators}
            >
              Operators
            </button>
            <button
              disabled={this.props.disableAdministrator}
              style={{ backgroundColor: this.props.bgColorAdministrator }}
              onClick={this.openAdministrators}
            >
              Administrators
            </button>
          </div>
        </div>
        </div>
      </div>
    );
  }
}
