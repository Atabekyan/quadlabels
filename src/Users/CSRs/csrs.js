import React, { Component } from "react";
import User from "../user.js";
import Select from "react-select";
import icon1 from "../User-Images/icon1.png";
import icon2 from "../User-Images/icon2.png";
import icon3 from "../User-Images/icon3.png";

export default class CSRs extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1, backgroundImage: "logo.png" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentCSRs">
        <div className="containerCommonCSRs">
          <User bgColorCSRS="#707070" disableCSRS="true" />
          <div className="containerInputCSRs">
            <div className="textAdministrator">
              <p>Customer Service Representatives</p>
            </div>
            <div className="containerAdministratorAdministrators">
              <p>CSR</p>
              <div className="containerSellersPermitInput">
                <Select options={options} />
              </div>
              <div className="containerCustomersCustomersButtons">
                <button>
                  <img src={icon1} alt="No Load Image" />
                </button>
                <button style={{ marginLeft: "10px;" }}>
                  <img src={icon2} alt="No Load Image" />
                </button>
                <button>
                  <img src={icon3} alt="No Load Image" />
                </button>
              </div>
            </div>
            <div className="containerNameAdministrators">
              <p>Name</p>
              <input className="cursorMove" placeholder="Pamela Fernandez"></input>
            </div>
            <div className="containerAddressAdministrators">
              <p>Address</p>
              <input className="cursorMove" placeholder="1151 Justin Ave"></input>
            </div>
            <div className="containerEmptyAdministrators">
              <p></p>
              <input className="cursorMove"></input>
            </div>
            <div className="containerCityAdministrators">
              <p>City</p>
              <input className="cursorMove" placeholder="Glendale"></input>
            </div>
            <div className="containerZipAdministrators">
              <p>Zip</p>
              <input className="cursorMove" placeholder="91201"></input>
            </div>
            <div className="containerStateAdministrators">
              <p>State</p>
              <input className="cursorMove" placeholder="CA"></input>
            </div>
            <div className="containerPhoneAdministrators">
              <p>Phone</p>
              <input className="cursorMove" placeholder="(818) 633 9094"></input>
            </div>
            <div className="containerEmailAdministartors">
              <p>e-mail</p>
              <input className="cursorMove" placeholder="tj@gmail.com"></input>
            </div>
            <div className="containerButtonSaveCSRs">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
