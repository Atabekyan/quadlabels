import React, { Component } from "react";
import User from "../user.js";
import Select from "react-select";
import icon1 from "../User-Images/icon1.png";
import icon2 from "../User-Images/icon2.png";
import icon3 from "../User-Images/icon3.png";

export default class Operator extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1, backgroundImage: "logo.png" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentOperator">
        <div className="containerCommonOperator">
          <User bgColorOperator="#707070" disableOperator="true" />
          <div className="containerInputCSRs">
            <div className="textAdministrator">
              <p>Operators</p>
            </div>
            <div className="containerAdministratorAdministrators">
              <p>Operator</p>
              <div className="containerSellersPermitInput">
                <Select options={options} />
              </div>
              <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
            </div>
            <div className="containerNameAdministrators">
              <p>Name</p>
              <input className="cursorMove" placeholder="Alex Navaro"></input>
            </div>
            <div className="containerAddressAdministrators">
              <p>Address</p>
              <input className="cursorMove" placeholder="34221 Monster Ave"></input>
            </div>
            <div className="containerEmptyAdministrators">
              <p></p>
              <input className="cursorMove"></input>
            </div>
            <div className="containerCityAdministrators">
              <p>City</p>
              <input className="cursorMove" placeholder="Santa Clarita"></input>
            </div>
            <div className="containerZipAdministrators">
              <p>Zip</p>
              <input className="cursorMove" placeholder="91354"></input>
            </div>
            <div className="containerStateAdministrators">
              <p>State</p>
              <input className="cursorMove" placeholder="CA"></input>
            </div>
            <div className="containerPhoneAdministrators">
              <p>Phone</p>
              <input className="cursorMove" placeholder="(818) 633 9094"></input>
            </div>
            <div className="containerEmailAdministartors">
              <p>e-mail</p>
              <input className="cursorMove" placeholder="tj@gmail.com"></input>
            </div>
            <div className="containerButtonSaveCSRs">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
