import React, { Component } from "react";
import Select from "react-select";
import logo from "../User-Images/logo.png";
import { Redirect } from 'react-router-dom';

export default class Calculator extends Component {
  constructor(props){
    super(props);
    this.state={
      isClickBack: false
    }
    this.backHome = this.backHome.bind(this);
  }
  backHome(){
    this.setState({
      isClickBack: true
  })
  }
  render() {
    const { isClickBack } = this.state;
    if(isClickBack){
      return <Redirect to ="/users/customers" />
    }
    const options = [
      { label: "Pacer Print", value: 1 },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentCalculator">
        <div className="containerLogoBack">
          <div className="containerLogo">
            <a href="/">
              <img src={logo} alt="No Load Image" />
            </a>
          </div>
          <div className="buttonBack">
            <button onClick={this.backHome}>Back</button>
          </div>
        </div>
        <div className="containerCalculator">
          <div className="containerhorizontalLine">
            <hr />
          </div>
          <div className="containerCalculatorInputs">
            <div className="textDiscountRateCalculator">
              <p>Discount Rate Calculator</p>
            </div>
            <div className="containerLongInputsCalculator">
              <div className="containerCustomersListCalculator">
                <p>Customers List</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <button>Update All</button>
              </div>
              <div className="containerCustomerCalculator">
                <p>Customer</p>
                <input className="cursorMove" placeholder="Pacer Print"></input>
              </div>
            </div>
            <div className="containerShortInputsCalculator">
              <div className="containerStartDateCalculator">
                <p>Start Date</p>
                <input className="cursorMove" placeholder="10021 Pietro Dr"></input>
                <button>Date Picker</button>
              </div>
              <div className="containerQualificationCalculator">
                <p>Qualification</p>
                <input className="cursorMove" placeholder="25%"></input>
              </div>
              <div className="containerMonthlyAverageSalesCalculator">
                <p>Monthly Average Sales [last 12 months]</p>
                <input className="cursorMove" placeholder="Pacer-logo.png"></input>
              </div>
              <div className="containerDiscountRate">
                <p>Discount Rate</p>
                <input className="cursorMove" placeholder="15.8%"></input>
                <button>Calculate</button>
              </div>
            </div>
            <div className="containerButtonSaveCalculator">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
