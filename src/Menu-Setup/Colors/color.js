import React, { Component } from "react";
import MenuSetup from "../menu-setup.js";
import Select from "react-select";
import icon1 from "../Menu-Setup_image/icon1.png";
import icon2 from "../Menu-Setup_image/icon2.png";
import icon3 from "../Menu-Setup_image/icon3.png";

export default class Color extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1 },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentColor">
        <div className="containerCommonColor">
          <MenuSetup bgColorColors="#707070" disableColors="true" />
          <div className="containerCommonColorInputs">
            <div className="containerColorMenuSetupLongInputs">
              <div className="textMaterialMenuSetup">
                <p>Color Menu Setup</p>
              </div>
              <div className="containerColorMenuColor">
                <p>Color menu</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameColor">
                <p>Name</p>
                <input className="cursorMove" placeholder="Full Color + White"></input>
              </div>
            </div>
            <div className="containerColorMenuSetupShortInputs">
              <div className="containerWhiteColor">
                <div className="containerWhiteColorCheckbox">
                  <p>White</p>
                  <input type="checkbox"></input>
                </div>
                <div className="containerWhiteInput">
                  <input className="cursorMove" placeholder="108.00"></input>
                  <label>USD/Liter</label>
                </div>
                <div className="containerWhiteAverageUsage">
                  <p>Average Usage</p>
                  <input className="cursorMove" placeholder="100"></input>
                  <label>%</label>
                </div>
              </div>
            </div>
            <div className="containerCyanColor">
              <div className="containerWhiteColorCheckbox">
                <p>Cyan</p>
                <input type="checkbox"></input>
              </div>
              <div className="containerCyanInput">
                <input className="cursorMove" placeholder="97.00"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerWhiteAverageUsage">
                <p>Average Usage</p>
                <input className="cursorMove" placeholder="30"></input>
                <label>%</label>
              </div>
            </div>
            <div className="containerMagentaColor">
              <div className="containerWhiteColorCheckbox">
                <p>Magenta</p>
                <input type="checkbox"></input>
              </div>
              <div className="containerMagentaInput">
                <input className="cursorMove" placeholder="97.00"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerWhiteAverageUsage">
                <p>Average Usage</p>
                <input className="cursorMove" placeholder="60"></input>
                <label>%</label>
              </div>
            </div>
            <div className="containerYellowColor">
              <div className="containerWhiteColorCheckbox">
                <p>Yellow</p>
                <input type="checkbox"></input>
              </div>
              <div className="containerYellowInput">
                <input className="cursorMove" placeholder="97.00"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerWhiteAverageUsage">
                <p>Average Usage</p>
                <input className="cursorMove" placeholder="80"></input>
                <label>%</label>
              </div>
            </div>
            <div className="containerBlackColor">
              <div className="containerWhiteColorCheckbox">
                <p>Black</p>
                <input type="checkbox"></input>
              </div>
              <div className="containerBlackInput">
                <input className="cursorMove" placeholder="98.00"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerWhiteAverageUsage">
                <p>Average Usage</p>
                <input className="cursorMove" placeholder="100"></input>
                <label>%</label>
              </div>
            </div>
            <div className="containerPantoneXColor">
              <div className="containerWhiteColorCheckbox">
                <p>Pantone X</p>
                <input type="checkbox"></input>
              </div>
              <div className="containerPantoneXInput">
                <input className="cursorMove" placeholder="97.00"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerWhiteAverageUsage">
                <p>Average Usage</p>
                <input className="cursorMove" placeholder="100"></input>
                <label>%</label>
              </div>
            </div>
            <div className="containerPantoneYColor">
              <div className="containerWhiteColorCheckbox">
                <p>Pantone Y</p>
                <input type="checkbox"></input>
              </div>
              <div className="containerPantoneXInput">
                <input className="cursorMove" placeholder="97.00"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerWhiteAverageUsage">
                <p>Average Usage</p>
                <input className="cursorMove" placeholder="100"></input>
                <label>%</label>
              </div>
            </div>
            <div className="containerPantoneZColor">
              <div className="containerWhiteColorCheckbox">
                <p>Pantone Z</p>
                <input type="checkbox"></input>
              </div>
              <div className="containerPantoneXInput">
                <input className="cursorMove" placeholder="98.00"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerWhiteAverageUsage">
                <p>Average Usage</p>
                <input className="cursorMove" placeholder="100"></input>
                <label>%</label>
              </div>
            </div>
            <div className="containerTotalCoverageColor">
              <div className="containerTotalCoverageColors">
                <p>W+CMYK Total Coverage</p>
                <input className="cursorMove" placeholder="320"></input>
                <label>%</label>
              </div>
            </div>
            <div className="containerCommentColor">
              <p>Info/Comments</p>
              <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
            </div>
            <div className="saveButtonMenuSetupMaterial">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
