import React, { Component } from "react";
import MenuSetup from "../menu-setup.js";
import Select from "react-select";
import icon1 from "../Menu-Setup_image/icon1.png";
import icon2 from "../Menu-Setup_image/icon2.png";
import icon3 from "../Menu-Setup_image/icon3.png";

export default class RollParameter extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1 },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentRollParameter">
        <div className="containerCommonRollParameter">
          <MenuSetup
            bgColorRollParameters="#707070"
            disableRollParameters="true"
          />
          <div className="containerRollParameterCommonInputs">
            <div className="textRollParameter">
              <p>Roll Parameters</p>
            </div>
            <div className="containerRollParametersFirstInputSection">
              <div className="containerRollFinishingRollParameters">
                <p>Roll Finishing</p>
                <div className="containerRollFinishingSelect">
                  <Select options={options} />
                </div>
                <div className="containerCustomersRollParametersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameRollParameters">
                <p>Name</p>
                <div className="containerRollFinishingInput">
                <input
                  className="cursorMove"
                  placeholder="Labels on Rolls"
                ></input>
                </div>
              </div>
              <div className="containerRollParametersFirstShortInputSection">
                <div className="containerCost">
                  <p>Cost</p>
                  <input className="cursorMove" placeholder="0.0"></input>
                  <label>USD/msi</label>
                </div>
                <div className="containerSetupCharge">
                  <p>Setup Charge</p>
                  <input className="cursorMove" placeholder="10"></input>
                  <label>USD</label>
                </div>
                <div className="containerPrefitPerData">
                  <p>Prefit per Data</p>
                  <input className="cursorMove" placeholder="0.0"></input>
                  <label>USD/msi</label>
                </div>
              </div>
              <div className="containerRollParametersComments">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
            </div>
            <div className="containerRollParametersLabelDirectionInputSection">
              <div className="containerLabelDirection">
                <p>Label Direction</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersRollParametersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameLabelDirection">
                <p>Name</p>
                <input
                  className="cursorMove"
                  placeholder="#4 Left Side First"
                ></input>
              </div>
              <div className="containerIconImageLabelDirection">
                <p>Icon Image</p>
                <input
                  className="cursorMove"
                  placeholder="Label-direction n4.png"
                ></input>
                <button>Browse</button>
              </div>
              <div className="conatinerCommentsLabelDirection">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
                <div className="buttonSaveLabelDirection">
                  <button>Save</button>
                </div>
            </div>
            <div className="containerRollParametersLabelsPerRoll">
              <div className="containerLabelsPerRollRollParameter">
                <p>Labels Per Roll</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersRollParametersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameLabelsPerRollRollRollParameter">
                <p>Name</p>
                <input
                  className="cursorMove"
                  placeholder="Labels on Rolls"
                ></input>
              </div>
              <div className="containerSetupChargeLabelsPerRollRollRollParameter">
                <p>Setup Charge</p>
                <input className="cursorMove" placeholder="0.5"></input>
                <label>USD/Roll</label>
              </div>
              <div className="containerProfitLabelsPerRollRollRollParameter">
                <p>Profit</p>
                <input className="cursorMove" placeholder="0.5"></input>
                <label>USD/Roll</label>
              </div>
              <div className="containerCommentsLabelsPerRollRollRollParameter">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
              <div className="buttonSaveLabelDirection">
              <hr className="redHorizontalLine"></hr>
                <button>Save</button>
              </div>
            </div>
            <div className="containerRollParametersMaximumOd">
              <div className="containerMaximumOdRollParameter">
                <p>Maximum OD</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersRollParametersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameMaximumOdRollParameter">
                <p>Name</p>
                <input className="cursorMove" placeholder="Maximum OD"></input>
              </div>
              <div className="containerDefaultMaxOdRollParameter">
                <p>Default Max. OD</p>
                <input className="cursorMove" placeholder="10"></input>
                <label>inch</label>
              </div>
              <div className="containerCommentsMaximumOdRollParameter">
                <p>Indo/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
              <div className="buttonSaveLabelDirection">
              <hr className="redHorizontalLine"></hr>
                <button>Save</button>
              </div>
            </div>
            <div className="containerCoreDiameterRollParameterInputs">
              <div className="containerCoreDiameterRollParameter">
                <p>Core Diameter</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersRollParametersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameCoreDiameterRollParameter">
                <p>Name</p>
                <input className="cursorMove" placeholder="1 inch"></input>
              </div>
              <div className="containerCostCoreDiameterRollParameter">
                <p>Cost</p>
                <input className="cursorMove" placeholder="0.012"></input>
                <label>USD/Roll</label>
              </div>
              <div className="containerCommentsCoreDiameterRollParameter">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
              <div className="buttonSaveLabelDirection">
              <hr className="redHorizontalLine"></hr>
                <button>Save</button>
              </div>
            </div>
            <div className="containerRollFinishingLastInputs">
              <div className="containerRollFinishingLast">
                <p>Roll Finishing</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersRollParametersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameRollFinishingLast">
                <p>Name</p>
                <input
                  className="cursorMove"
                  placeholder="Cut Sheet Labels"
                ></input>
              </div>
              <div className="containerCostRollFinishingLast">
                <p>Cost</p>
                <input className="cursorMove" placeholder="0.0"></input>
                <label>USD/msi</label>
              </div>
              <div className="SetupChargeRollFinishingLast">
                <p>Setup Charge</p>
                <input className="cursorMove" placeholder="10"></input>
                <label>USD</label>
              </div>
              <div className="containerProfitPerDataRollFinishingLast">
                <p>Profit per Data</p>
                <input className="cursorMove" placeholder="0.0"></input>
                <label>USD</label>
              </div>
              <div className="containerCommentsRollFinishingLast">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
              <div className="containerButtonSaveRollParameter">
                <button>Save</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
