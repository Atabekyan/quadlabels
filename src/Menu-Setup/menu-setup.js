import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Back from "../ButtonBack/back.js";
import Logo from "../Logo/logo.js";
import logo from "./Menu-Setup_image/logo.png";

export default class MenuSetup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isClickOpenMaterial: false,
      isClickOpenColor: false,
      isClickOpenDies: false,
      isClickOpenSurfaceFinishing: false,
      isClickOpenFoilStamping: false,
      isClickOpenSpotUVVarnish: false,
      isClickOpenVariableData: false,
      isClickOpenOrderData: false,
      isClickOpenRollParameters: false
    };
    this.openMaterial = this.openMaterial.bind(this);
    this.openColors = this.openColors.bind(this);
    this.openDies = this.openDies.bind(this);
    this.openSurfaceFinishing = this.openSurfaceFinishing.bind(this);
    this.openFoilStamping = this.openFoilStamping.bind(this);
    this.openSpotUVVarnish = this.openSpotUVVarnish.bind(this);
    this.openVariableData = this.openVariableData.bind(this);
    this.openOrderData = this.openOrderData.bind(this);
    this.openRollParameters = this.openRollParameters.bind(this);
  }
  openMaterial() {
    this.setState({
      isClickOpenMaterial: true
    });
  }
  openColors() {
    this.setState({
      isClickOpenColor: true
    });
  }
  openSurfaceFinishing() {
    this.setState({
      isClickOpenSurfaceFinishing: true
    });
  }
  openDies() {
    this.setState({
      isClickOpenDies: true
    });
  }
  openFoilStamping() {
    this.setState({
      isClickOpenFoilStamping: true
    });
  }
  openSpotUVVarnish() {
    this.setState({
      isClickOpenSpotUVVarnish: true
    });
  }
  openVariableData() {
    this.setState({
      isClickOpenVariableData: true
    });
  }
  openOrderData() {
    this.setState({
      isClickOpenOrderData: true
    });
  }
  openRollParameters() {
    this.setState({
      isClickOpenRollParameters: true
    });
  }
  render() {
    const {
      isClickBack,
      isClickOpenMaterial,
      isClickOpenColor,
      isClickOpenDies,
      isClickOpenSurfaceFinishing,
      isClickOpenFoilStamping,
      isClickOpenSpotUVVarnish,
      isClickOpenVariableData,
      isClickOpenOrderData,
      isClickOpenRollParameters
    } = this.state;
    if (isClickOpenMaterial) {
      return <Redirect to="/menu-setup/materials" />;
    }
    if (isClickOpenColor) {
      return <Redirect to="/menu-setup/colors" />;
    }
    if (isClickOpenDies) {
      return <Redirect to="/menu-setup/dies" />;
    }
    if (isClickOpenSurfaceFinishing) {
      return <Redirect to="/menu-setup/surface-finishing" />;
    }
    if (isClickOpenFoilStamping) {
      return <Redirect to="/menu-setup/foil-stamping" />;
    }
    if (isClickOpenSpotUVVarnish) {
      return <Redirect to="/menu-setup/spot-uv-varnish" />;
    }
    if (isClickOpenVariableData) {
      return <Redirect to="/menu-setup/variable-data" />;
    }
    if (isClickOpenOrderData) {
      return <Redirect to="/menu-setup/order-data" />;
    }
    if (isClickOpenRollParameters) {
      return <Redirect to="/menu-setup/roll-parameter" />;
    }
    return (
      <div className="contentMenuSetup">
        <div className="containerLogoBack">
          <div className="containerLogo">
            <a href="/">
              <img src={logo} alt="No Load Image" />
            </a>
          </div>
          <Back />
        </div>
        <div className="containerCommonMenuSetup">
        <div className="textMenuSetup">
          <p>Menu Setup</p>
        </div>
          <div className="containerMaterialSetupButtons">
            <button
              disabled={this.props.disableMaterial}
              style={{ backgroundColor: this.props.bgColorMaterial }}
              onClick={this.openMaterial}
            >
              Material
            </button>
            <button
              disabled={this.props.disableColors}
              style={{ backgroundColor: this.props.bgColorColors }}
              onClick={this.openColors}
            >
              Colors
            </button>
            <button
              disabled={this.props.disableDies}
              style={{ backgroundColor: this.props.bgColorDies }}
              onClick={this.openDies}
            >
              Dies
            </button>
            <button
              disabled={this.props.disableSurfaceFinishing}
              style={{ backgroundColor: this.props.bgColorSurfaceFinishing }}
              onClick={this.openSurfaceFinishing}
            >
              Surface Finishing
            </button>
            <button
              disabled={this.props.disableFoilStamping}
              style={{ backgroundColor: this.props.bgColorFoilStamping }}
              onClick={this.openFoilStamping}
            >
              Foil Stamping
            </button>
            <button
              disabled={this.props.disableSpotVarnish}
              style={{ backgroundColor: this.props.bgColorSpotVarnish }}
              onClick={this.openSpotUVVarnish}
            >
              Spot UV Varnish
            </button>
            <button
              disabled={this.props.disableVariableData}
              style={{ backgroundColor: this.props.bgColorVariableData }}
              onClick={this.openVariableData}
            >
              Variable data
            </button>
            <button
              disabled={this.props.disableRollParameters}
              style={{ backgroundColor: this.props.bgColorRollParameters }}
              onClick={this.openRollParameters}
            >
              Roll Parameters
            </button>
            <button
              disabled={this.props.disableOrderData}
              style={{ backgroundColor: this.props.bgColorOrderData }}
              onClick={this.openOrderData}
            >
              Order Data
            </button>
          </div>
        </div>
      </div>
    );
  }
}
