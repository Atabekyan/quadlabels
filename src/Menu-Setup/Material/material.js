import React, { Component } from "react";
import MenuSetup from "../menu-setup.js";
import Select from "react-select";
import icon1 from "../Menu-Setup_image/icon1.png";
import icon2 from "../Menu-Setup_image/icon2.png";
import icon3 from "../Menu-Setup_image/icon3.png";

export default class Material extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1, backgroundImage: "logo.png" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentMaterial">
        <div className="containerCommonMaterial">
          <MenuSetup bgColorMaterial="#707070" disableMaterial="true" />
          <div className="containerMaterialCommonInputs">
            <div className="containerMaterialMenuSetupLongInputs">
              <div className="textMenuSetupMaterialMenuSetup">
                <p>Material Menu Setup</p>
              </div>
              <div className="containerCustomersCustomers">
                <p>Material menu</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameMaterial">
                <p>Name</p>
                <input className="cursorMove" placeholder="High-Gloss Paper"></input>
              </div>
              <div className="containerCustomersCustomers">
                <p>Adhesive Type</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerAdhesiveTypeInputMaterial">
                <p>Adhesive Type</p>
                <input className="cursorMove" placeholder="Normal"></input>
              </div>
            </div>
            <div className="containerMaterialMenuSetupShortInputs">
              <div className="containerWidthMaterial">
                <p>Width</p>
                <input className="cursorMove" placeholder="13"></input>
                <label>inch</label>
              </div>
              <div className="containerLengthMaterial">
                <p>Length</p>
                <input className="cursorMove" placeholder="5000"></input>
                <label>ft</label>
              </div>
              <div className="containerThicknessMaterial">
                <p>Thickness</p>
                <input className="cursorMove" placeholder="2.6"></input>
                <label>mil</label>
              </div>
              <div className="containerWeightMaterial">
                <p>Weight</p>
                <input className="cursorMove" placeholder="0.175"></input>
                <label>lb/msi</label>
              </div>
              <div className="containerWasteMaterial">
                <p>Waste</p>
                <input className="cursorMove" placeholder="60"></input>
                <label>ft</label>
              </div>
              <div className="containerPriceMaterial">
                <p>Price</p>
                <input className="cursorMove" placeholder="0.564"></input>
                <label>USD</label>
              </div>
              <div className="containerInfoCommentsMaterial">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments" type="text"></textarea>
              </div>
            </div>
            <div className="saveButtonMenuSetupMaterial">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
