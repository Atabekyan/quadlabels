import React, { Component } from "react";
import MenuSetup from "../menu-setup.js";
import Select from "react-select";
import icon1 from "../Menu-Setup_image/icon1.png";
import icon2 from "../Menu-Setup_image/icon2.png";
import icon3 from "../Menu-Setup_image/icon3.png";

export default class Dies extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1, backgroundImage: "logo.png" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentDies">
        <div className="containerCommonDies">
          <MenuSetup bgColorDies="#707070" disableDies="true" />
          <div className="containerDiesCommonInputs">
            <div className="containerDiesLongInputs">
              <div className="textDies">
                <p>Dies</p>
              </div>
              <div className="containerShape">
                <p>Shape</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerShapeName">
                <p>Shape Name</p>
                <input className="cursorMove" placeholder="Rectagle"></input>
              </div>
              <div className="containerDie">
                <p>Die</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameDies">
                <p>Name</p>
                <input className="cursorMove" placeholder="R-2_2500X4_5000"></input>
              </div>
            </div>
            <div className="containerDiesShortInputs">
              <div className="containerHeight">
                <p>Height</p>
                <input className="cursorMove" placeholder="2.25"></input>
                <label>inch</label>
              </div>
              <div className="containerWidthDies">
                <p>Width</p>
                <input className="cursorMove" placeholder="4.5"></input>
                <label>inch</label>
              </div>
              <div className="containerCornerRounding">
                <p>Corner Rounding</p>
                <input className="cursorMove" placeholder="0.05"></input>
                <label>inch</label>
              </div>
              <div className="containerNumberAcross">
                <p>Number Across</p>
                <input className="cursorMove" placeholder="5"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerNumberAround">
                <p>Number Around</p>
                <input className="cursorMove" placeholder="2"></input>
                <label>USD/Liter</label>
              </div>
              <div className="containerCenterToCenterAcross">
                <p>Center to Center Across</p>
                <input className="cursorMove" placeholder="100"></input>
                <label>mm</label>
                <div className="containerGapAcross">
                  <p>Gap Across</p>
                  <input className="cursorMove" placeholder="0.012500"></input>
                  <label>inch</label>
                </div>
              </div>
              <div className="containerCenterToCenterAround">
                <p>Center to Center Around</p>
                <input className="cursorMove" placeholder="0.30"></input>
                <label>mm</label>
                <div
                  className="containerGapAround"
                  className="containerGapAcross"
                >
                  <p>Gap Around</p>
                  <input className="cursorMove" placeholder="0.012819"></input>
                  <label>inch</label>
                </div>
              </div>
              <div className="containerThicknessDies">
                <p>Thickness</p>
                <input className="cursorMove" placeholder="0.150"></input>
                <label>mm</label>
              </div>
              <div className="containerPrice">
                <p>Price</p>
                <input className="cursorMove" placeholder="140.00"></input>
                <label>USD</label>
              </div>
              <div className="containerDate">
                <p>Date</p>
                <input className="cursorMove" placeholder="09/30/2018"></input>
                <button>Browse</button>
              </div>
              <div className="containerCommentsDies">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
            </div>
            <div className="saveButtonDies">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
