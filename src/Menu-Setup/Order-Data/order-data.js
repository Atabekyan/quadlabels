import React, { Component } from "react";
import MenuSetup from "../menu-setup.js";
import Select from "react-select";
import icon1 from "../Menu-Setup_image/icon1.png";
import icon2 from "../Menu-Setup_image/icon2.png";
import icon3 from "../Menu-Setup_image/icon3.png";

export default class OrderData extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1, backgroundImage: "logo.png" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentOrderData">
        <div className="containerCommonOrder">
          <MenuSetup bgColorOrderData="#707070" disableOrderData="true" />
          <div className="containerCommonInputsOrderData">
            <div className="textOrderDataMenu-Setup">
              <p>Order Data</p>
            </div>
            <div className="containerOrderDataLongInputs">
              <div className="containerCustomersCustomers">
                <p>Product Sample</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerName">
                <p>Name</p>
                <input className="cursorMove" placeholder="Product Sample"></input>
              </div>
            </div>
            <div className="containerOrderDataShortInputs">
              <div className="containerFixedPrice">
                <p>Fixed Price</p>
                <input className="cursorMove" placeholder="75"></input>
                <label>USD</label>
              </div>
              <div className="containerMinFree">
                <p>Min. Free</p>
                <input className="cursorMove" placeholder="4"></input>
                <label>Designs</label>
              </div>
              <div className="containerProfitPerSample">
                <p>Profit per Sample</p>
                <input className="cursorMove" placeholder="0"></input>
                <label>USD/Sample</label>
              </div>
              <div className="containerCommentOrderData">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
            </div>
            <div className="textArtworks">
              <p>Artwork(s)</p>
            </div>
            <div className="containerArtworksInputs">
              <div className="containerArtworksMinFree">
                <p>Min. Free</p>
                <input className="cursorMove" placeholder="4"></input>
                <label>Artworks</label>
              </div>
              <div className="containerArtworkCharge">
                <p>Artwork Charge</p>
                <input className="cursorMove" placeholder="4.00"></input>
                <label>USD</label>
              </div>
              <div className="containerArtworksProfit">
                <p>Profit</p>
                <input className="cursorMove" placeholder="2.50"></input>
                <label>USD/Artwork</label>
              </div>
              <div className="containerArtworksComments">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
            </div>
            <div className="saveButtonMenuSetupMaterial">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
