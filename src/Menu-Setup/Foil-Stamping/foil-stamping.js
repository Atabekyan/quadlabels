import React, { Component } from "react";
import MenuSetup from "../menu-setup.js";
import Select from "react-select";
import icon1 from "../Menu-Setup_image/icon1.png";
import icon2 from "../Menu-Setup_image/icon2.png";
import icon3 from "../Menu-Setup_image/icon3.png";

export default class FoilStamping extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1, backgroundImage: "logo.png" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentFoilStamping">
        <MenuSetup bgColorFoilStamping="#707070" disableFoilStamping="true" />
        <div className="containerMaterialCommonInputs">
          <div className="textMaterialMenuSurfaceFinishing">
            <p>Foil Stamping</p>
          </div>
          <div className="containerSurfaceFinishingLongInputs">
            <div className="containerSurfaceFinishingInputFinishing">
              <p>Foil Stamping</p>
              <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
              <div className="containerCustomersCustomersButtons">
                <button>
                  <img src={icon1} alt="No Load Image" />
                </button>
                <button style={{ marginLeft: "10px;" }}>
                  <img src={icon2} alt="No Load Image" />
                </button>
                <button>
                  <img src={icon3} alt="No Load Image" />
                </button>
              </div>
            </div>
            <div className="containerSurfaceFinishingName">
              <p>Name</p>
              <input className="cursorMove" placeholder="Matte Lamination"></input>
            </div>
          </div>
          <div className="containerSurfaceFinishingShortInputs">
            <div className="containerSurfaceFinishingWidth">
              <p>Width</p>
              <input className="cursorMove" placeholder="12.75"></input>
              <label>inch</label>
            </div>
            <div className="containerSurfaceFinishingLength">
              <p>Length</p>
              <input className="cursorMove" placeholder="10000"></input>
              <label>ft</label>
            </div>
            <div className="containerSurfaceFinishingThickness">
              <p>Thickness</p>
              <input className="cursorMove" placeholder="35"></input>
              <label>mil</label>
            </div>
            <div className="containerSurfaceFinishingWeight">
              <p>Weight</p>
              <input className="cursorMove" placeholder="0.0012"></input>
              <label>lb/msi</label>
            </div>
            <div className="containerSurfaceFinishingWaste">
              <p>Waste</p>
              <input className="cursorMove" placeholder="100"></input>
              <label>ft</label>
            </div>
            <div className="containerSurfaceFinishingCost">
              <p>Cost</p>
              <input className="cursorMove" placeholder="0.87"></input>
              <label>USD/msi</label>
            </div>
            <div className="containerSurfaceFinishingSetupCharge">
              <p>Setup Charge</p>
              <input className="cursorMove" placeholder="150.00"></input>
              <label>USD</label>
            </div>
            <div className="containerSurfaceFinishingProfitPerMsi">
              <p>Profit per msi</p>
              <input className="cursorMove" placeholder="1.02"></input>
              <label>USD/msi</label>
            </div>
            <div className="containerCommentSurfaceFinishing">
              <p>info/Comments</p>
              <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
            </div>
          </div>
          <div className="saveButtonMenuSetupMaterial">
          <button>Save</button>
        </div>
        </div>
      </div>
    );
  }
}
