import React, { Component } from "react";
import MenuSetup from "../menu-setup.js";
import Select from "react-select";
import icon1 from "../Menu-Setup_image/icon1.png";
import icon2 from "../Menu-Setup_image/icon2.png";
import icon3 from "../Menu-Setup_image/icon3.png";

export default class VariableData extends Component {
  render() {
    const options = [
      { label: "Pacer Print", value: 1, backgroundImage: "logo.png" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentVariableData">
        <div className="containerCommonVariableData">
          <MenuSetup bgColorVariableData="#707070" disableVariableData="true" />
          <div className="containerVarriableDataCommonInput">
            <div className="textVariableData">
              <p>Variable Data</p>
            </div>
            <div className="containerVariableDataLongInputs">
              <div className="containerVariableDatas">
                <p>Variable Data</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
                <div className="containerCustomersCustomersButtons">
                  <button>
                    <img src={icon1} alt="No Load Image" />
                  </button>
                  <button style={{ marginLeft: "10px;" }}>
                    <img src={icon2} alt="No Load Image" />
                  </button>
                  <button>
                    <img src={icon3} alt="No Load Image" />
                  </button>
                </div>
              </div>
              <div className="containerNameVariableData">
                <p>Name</p>
                <input className="cursorMove" placeholder="Alphanumeric"></input>
              </div>
            </div>
            <div className="containerVariableDataShortInputs">
              <div className="containerCostVariableData">
                <p>Cost</p>
                <input className="cursorMove" placeholder="0.05"></input>
                <label>USD/number</label>
              </div>
              <div className="containerSetupChargeVariableData">
                <p>Setup Charge</p>
                <input className="cursorMove" placeholder="150.00"></input>
                <label>USD</label>
              </div>
              <div className="containerProfitPerDataVariableData">
                <p>Profit per Data</p>
                <input className="cursorMove" placeholder="0.112"></input>
                <label>USD/number</label>
              </div>
              <div className="containerCommentsVariableData">
                <p>Info/Comments</p>
                <textarea className="cursorMoveTop" placeholder="Info/Comments"></textarea>
              </div>
            </div>
            <div className="saveButtonMenuSetupMaterial">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
