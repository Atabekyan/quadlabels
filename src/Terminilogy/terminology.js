import React, { Component } from "react";
import Back from "../ButtonBack/back.js";
import Select from "react-select";

export default class Terminology extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const options = [
      { label: "Pacer Print", value: 1, backgroundImage: "logo.png" },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentTerminology">
        <div className="containerLogoBack">
          <div className="containerLogo">
            <a href="/">
              <img src="logo.png" alt="No Load Image" />
            </a>
          </div>
          <Back />
        </div>
        <div className="textTerminology">
          <p>Terminology</p>
        </div>
        <div className="containerCommonTermonology">
          <div className="containerTerminologyButtons">
            <button>Title</button>
            <button id="label">Label</button>
            <button>Edit Box</button>
            <button>Button</button>
            <button>Text</button>
            <button>Check Button</button>
            <button>Email Termplates</button>
            <button>---</button>
          </div>
            <div className="continerTerminologySelectInputs">
              <div className="textLanguageTerminology">
                <p>Language/Terminology</p>
              </div>
              <div className="labelsUsedIn">
                <p>Labels used in</p>
                <div className="containerSellersPermitInput">
                  <Select options={options} />
                </div>
              </div>
              <div className="containerCommonInputsTerminology">
                <div className="containerTerminologyTextInputs">
                  <div className="containerDefault">
                    <p className="textDefault">Default</p>
                    <p>Normal</p>
                    <p>Width</p>
                    <p>Length</p>
                    <p>Thickness</p>
                    <p>Weight</p>
                    <p>Waste</p>
                  </div>
                  <div className="dds">
                    <div className="containerLanguage">
                      <p className>English</p>
                      <input type="checkbox"></input>
                      <p>Spanish</p>
                      <input type="checkbox"></input>
                      <p>French</p>
                      <input type="checkbox"></input>
                    </div>
                    <div className="containerLanguageInput">
                      <div className="containerEnglish">
                        <input className="cursorMove" placeholder="Normal"></input>
                        <input className="cursorMove" placeholder="Width"></input>
                        <input className="cursorMove" placeholder="Length"></input>
                        <input className="cursorMove" placeholder="Gauge"></input>
                        <input className="cursorMove" placeholder="Mass"></input>
                        <input className="cursorMove" placeholder="Trash"></input>
                      </div>
                      <div className="containerSpanish">
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                      </div>
                      <div className="containerFrench">
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                        <input className="cursorMove"></input>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="saveButtonTerminology">
                  <button>Save</button>
                </div>
              </div>
            </div>
        </div>
      </div>
    );
  }
}
