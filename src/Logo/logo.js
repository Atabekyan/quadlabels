import React, { Component } from "react";

export default class Logo extends Component {
  render() {
    return (
      <div className="contentLogo">
        <div className="containerLogo">
          <a href="/"><img src="logo.png" alt="No Load Image" /></a>
        </div>
      </div>
    );
  }
}
