import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Back from "../ButtonBack/back.js";
import logo from '../General-Setup/General-Setup-Images/logo.png';

export default class GeneralSetup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenUtilities: false,
      isOpenProfability: false,
      isOpenProductionSets: false
    };
    this.openProfability = this.openProfability.bind(this);
    this.openProductionSets = this.openProductionSets.bind(this);
    this.openUtilities = this.openUtilities.bind(this);
  }
  openUtilities() {
    this.setState({
      isOpenUtilities: true
    });
  }
  openProfability() {
    this.setState({
      isOpenProfability: true
    });
  }
  openProductionSets() {
    this.setState({
      isOpenProductionSets: true
    });
  }
  render() {
    const {
      isOpenUtilities,
      isOpenProfability,
      isOpenProductionSets
    } = this.state;
    if (isOpenUtilities) {
      return <Redirect to="/general-setup/utilities" />;
    }

    if (isOpenProfability) {
      return <Redirect to="/general-setup/profabilities" />;
    }
    if (isOpenProductionSets) {
      return <Redirect to="/general-setup/production-sets" />;
    }
    return (
      <div className="contentGeneralSetup">
        <div className="containerLogoBack">
        <div className="containerLogo">
            <a href="/">
              <img src={logo} alt="No Load Image" />
            </a>
          </div>
          <Back />
        </div>
        <div className="containerGeneralSetupTextButtons">
        <div className="textGeneralSetup">
          <p>General Setup</p>
        </div>
        <div className="containerGeneralSetupButtons">
        <div className="containerhorizontalLine">
            <hr />
          </div>
          <div className="GeneralSetupButtons">
          <button
            disabled={this.props.disableUtilities}
            style={{ backgroundColor: this.props.bgColorUtilities }}
            onClick={this.openUtilities}
          >
            Utilities/Expences
          </button>
          <button
            disabled={this.props.disableRoi}
            style={{ backgroundColor: this.props.bgColorRoi }}
            onClick={this.openProfability}
          >
            Profitability/ROI
          </button>
          <button
            disabled={this.props.disableProductionSets}
            style={{ backgroundColor: this.props.bgColorProductionSets }}
            onClick={this.openProductionSets}
          >
            Production Sets
          </button>
          </div>
        </div>
        </div>
      </div>
    );
  }
}
