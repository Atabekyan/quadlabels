import React, { Component } from "react";
import GeneralSetup from "../general-setup";

export default class Profitability extends Component {
  render() {
    return (
      <div className="contentProfitability">
        <GeneralSetup bgColorRoi="#707070" disableRoi="true" />
        <div className="containerProfabilityChartInputs">
          <div className="textProfabilityChart">
            <p>Profability Chart</p>
          </div>
          <div className="containerShortRun">
            <p>Short Run</p>
            <div className="containerShortRunFirstInput">
              <input className="cursorMove" placeholder="250"></input>
            </div>
            <input className="cursorMove" className="profabilityChartSecondInputs" placeholder="1800"></input>
            <label>USD</label>
          </div>
          <div className="containerS-MRun">
            <p>S-M Run</p>
            <div className="containerS-MRunFirstInput">
              <input className="cursorMove" placeholder="10000"></input>
            </div>
            <input className="cursorMove" className="profabilityChartSecondInputs" placeholder="1200"></input>
            <label>USD</label>
          </div>
          <div className="containerM-LRunProfability">
            <p>M-L Run</p>
            <div className="containerM-LRunFirstInput">
              <input className="cursorMove" placeholder="10000"></input>
            </div>
            <input className="cursorMove" className="profabilityChartSecondInputs" placeholder="800"></input>
            <label>USD</label>
          </div>
          <div className="containerLongRunProfability">
            <p>Long Run</p>
            <div className="containerLongRunFirstInput">
              <input className="cursorMove" placeholder="10000"></input>
            </div>
            <input className="cursorMove" className="profabilityChartSecondInputs" placeholder="700"></input>
            <label>USD</label>
          </div>
          <div className="textInvestment">
            <p>Investment</p>
          </div>
          <div className="containerInvestmentInputs">
            <div className="containerLeasingMonthlyPayments">
              <p>Leasing Monthly Payments</p>
              <input className="cursorMove" placeholder="60000"></input>
              <label>USD</label>
            </div>
            <div className="containerCashInvestment">
              <p>Cash Investment</p>
              <input className="cursorMove" placeholder="2500000"></input>
              <label>USD</label>
            </div>
            <div className="containerInventoryProfability">
              <p>Inventory</p>
              <input className="cursorMove" placeholder="300000"></input>
              <label>USD</label>
            </div>
          </div>
          <div className="textRoiChart">
            <p>ROI Chart</p>
          </div>
          <div className="containerRoiChartInputs">
            <div className="containerFirstMonthRevenue">
              <p>First Month Revenue</p>
              <input className="cursorMove" placeholder="420000"></input>
              <label>USD</label>
            </div>
            <div className="containerAnnualProjectedRevenue">
              <p>Annual Projected Revenue</p>
              <input className="cursorMove" placeholder="5400000"></input>
              <label>USD</label>
            </div>
            <div className="containerAverageMonthlyPotential">
              <p>Average Monthly Potential</p>
              <input className="cursorMove" placeholder="300000"></input>
              <label>USD</label>
            </div>
          </div>
          <div className="containerButtonSaveLocation">
            <button>Save</button>
          </div>
        </div>
      </div>
    );
  }
}
