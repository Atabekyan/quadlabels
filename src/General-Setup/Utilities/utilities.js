import React, { Component } from "react";
import GeneralSetup from "../general-setup.js";

export default class Utilities extends Component {
  render() {
    return (
      <div className="contentUtilities">
        <GeneralSetup bgColorUtilities="#707070" disableUtilities="true" />
        <div className="containerUtilitiesInputs">
          <div className="textUtilities">
            <p>Utilities</p>
          </div>
          <div className="containerSalariesUtilities">
            <p>Salaries</p>
            <input className="cursorMove" placeholder="180000"></input>
            <label>USD</label>
          </div>
          <div className="containerPayrolUtilities">
            <p>Payrol</p>
            <input className="cursorMove" placeholder="12000"></input>
            <label>USD</label>
          </div>
          <div className="containerFacilityRentUtilities">
            <p>Facility rent</p>
            <input className="cursorMove" placeholder="28000"></input>
            <label>USD</label>
          </div>
          <div className="containerCommunicationUtilities">
            <p>Communication</p>
            <input className="cursorMove" placeholder="2400"></input>
            <label>USD</label>
          </div>
          <div className="containerElectricityUtilities">
            <p>Electricity</p>
            <input className="cursorMove" placeholder="6000"></input>
            <label>USD</label>
          </div>
          <div className="containerOtherUtilities">
            <p>Other</p>
            <input className="cursorMove" placeholder="5000"></input>
            <label>USD</label>
          </div>
          <div className="containerButtonSaveLocation">
            <button>Save</button>
          </div>
        </div>
      </div>
    );
  }
}
