import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { isObjectExpression } from "@babel/types";
import GeneralSetup from "../general-setup";

export default class ProductionSets extends Component {
  render() {
    return (
      <div className="contentProductionSets">
        <GeneralSetup
          bgColorProductionSets="#707070"
          disableProductionSets="true"
        />
          <div className="containerUtilitiesInputs">
            <div className="textProductionSets">
              <p>Production Sets</p>
            </div>
            <div className="containerQuantityProduction">
              <p className="productionSetsTextWidth">Quantity</p>
                <input className="cursorMove" placeholder="3"></input>
                <label>sets</label>
            </div>
            <div className="containerMaximumProductionLoad">
              <p className="productionSetsTextWidth">Maximum Production Load</p>
                <input className="cursorMove" placeholder="67"></input>
                <label>%</label>
            </div>
            <div className="containerMaximumPrintWidth">
              <p className="productionSetsTextWidth">Maximum Printable Width</p>
                <input className="cursorMove" placeholder="322"></input>
                <label>mm</label>
            </div>
            <div className="containerMaximumPrintRepeat">
              <p className="productionSetsTextWidth">Maximum Print Repeat</p>
                <input className="cursorMove" placeholder="622"></input>
                <label>mm</label>
            </div>
            <div className="containerInkWaste">
              <p className="productionSetsTextWidth">Ink Waste</p>
                <input className="cursorMove" placeholder="10"></input>
                <label>liters/month</label>
            </div>
            <div className="containerProductionSpeed">
              <p className="productionSetsTextWidth">Production speed [CMYK]</p>
                <input className="cursorMove" placeholder="160"></input>
                <label>ft/min</label>
            </div>
            <div className="containerProductionSpeedW">
              <p className="productionSetsTextWidth">Production speed [CMYK+W]</p>
                <input className="cursorMove" placeholder="90"></input>
                <label>ft/min</label>
            </div>
            <div className="containerProductionSetupTime">
              <p className="productionSetsTextWidth">Production Setup Time</p>
              <div className="containerScolor">
                <p>Scolor</p>
                  <input className="cursorMove" placeholder="25"></input>
                  <label>min</label>
              </div>
              <div className="containerFcolor">
                <p>Fcolor</p>
                  <input className="cursorMove" placeholder="1"></input>
                  <label>min</label>
              </div>
            </div>
            <div className="containerButtonSaveLocation">
              <button>Save</button>
            </div>
          </div>
      </div>
    );
  }
}
