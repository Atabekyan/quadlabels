import React, { Component } from "react";
import Back from "../ButtonBack/back.js";
import Logo from "../Logo/logo.js";
import Select from "react-select";

export default class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locationAddress: "",
      locationCity: "",
      locationZip: "",
      locationState: "",
      locationContactPerson: "",
      locationPhone: "",
      locationEmail: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log(this.state.locationAddress);
  }
  render() {
    const options = [
      { label: "Pacer Print", value: 1 },
      { label: "Pacer Trint", value: 1 },
      { label: "Pacer Srint", value: 1 },
      { label: "Pacer Vrint", value: 1 }
    ];
    return (
      <div className="contentLocation">
        <div className="containerLogoBack">
          <Logo />
          <Back />
        </div>
        <div className="containerCommonLocation">
          <div className="textLocation">
            <p>Locations</p>
          </div>
          <div className="containerInputCSRs">
            <div className="containerCustomerLocation">
              <p>Customer</p>
              <div className="containerSellersPermitInput">
                <Select options={options} />
              </div>
            </div>
            <div className="containerLocationsLocation">
              <p>Locations</p>
              <div className="containerSellersPermitInput">
                <Select options={options} />
              </div>
            </div>
            <div className="containerAddressLocation">
              <p>Address</p>
              <input className="cursorMove"></input>
            </div>
            <div className="containerEmptyLocation">
              <p></p>
              <input className="cursorMove"></input>
            </div>
            <div className="containerCityLocation">
              <p>City</p>
              <input className="cursorMove" placeholder="Glendale"></input>
            </div>
            <div className="containerCityLocation">
              <p>ZIP</p>
              <input className="cursorMove" placeholder="91201"></input>
            </div>
            <div className="containerStateLocation">
              <p>State</p>
              <input className="cursorMove" placeholder="CA"></input>
            </div>
            <div className="containerContactPersonLocation">
              <p>Contact person</p>
              <input className="cursorMove" placeholder="Tom Johns"></input>
            </div>
            <div className="containerPhoneLocation">
              <p>Phone</p>
              <input
                className="cursorMove"
                placeholder="(818) 633 9094"
              ></input>
            </div>
            <div className="containerEmailLocation">
              <p>e-mail</p>
              <input className="cursorMove" placeholder="tj@gmail.com"></input>
            </div>
            <div className="containerButtonSaveLocation">
              <button>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
